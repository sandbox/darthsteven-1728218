<?php


class FeedsXpathParserXMLReaderParser extends FeedsXPathParserBase {

  /**
   * Override this method *just* to add our own custom logging.
   */
  protected function namespacedXPathQuery($xml, $query) {
    // Ensure we don't pick up someone else's errors
    libxml_clear_errors();
    list($results, $error) = $this->_query($xml, $query);

    if (is_object($error) && $this->source_config['exp']['errors']) {

      if ($error->level == LIBXML_ERR_ERROR) {
        $e = new iExploreImportError(t("There was an error with the XPath query: %query.<br>
          Libxml returned the message: %message, with the error code: %code.",
          array('%query'   => $query,
                '%message' => trim($error->message),
                '%code'    => $error->code)));
        $e->log_type = 'xml_error';
        libxml_clear_errors();
        throw $e;
      }
      elseif ($error->level == LIBXML_ERR_WARNING) {
        $e = new iExploreImportError(t("There was a warning with the XPath query: %query.<br>
          Libxml returned the message: %message, with the error code: %code.",
          array('%query'   => $query,
                '%message' => trim($error->message),
                '%code'    => $error->code)));
        $e->log_type = 'xml_error';
        libxml_clear_errors();
        throw $e;
      }
    }
    if ($results === FALSE) {
      return array();
    }
    return $results;
  }

  /**
   * Define defaults.
   */
  public function configDefaults() {
    return array_merge(array(
      'xml_reader_context' => '',
    ), parent::configDefaults());
  }

  /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);

    if (empty($source_config)) {
      $source_config = $this->config;
    }

    $form['xpath']['xml_reader_context'] = array(
      '#type' => 'textfield',
      '#title' => t('XMLReader Context'),
      '#required' => TRUE,
      '#description' => t('This is a / separated context selector.'),
      '#default_value' => isset($source_config['xml_reader_context']) ? $source_config['xml_reader_context'] : '',
      '#maxlength' => 1024,
      '#size' => 80,
      '#weight' => -100,
    );
    return $form;
  }

  /**
   * Override to return NULL when the element was absent from the XML instead
   * of ''. This allows us to distinguish between included but empty values
   * and completely absent values.
   *
   * @see _iexplore_import_feeds_preprocess_set_target().
   */
  public function getSourceElement(FeedsSource $source, FeedsParserResult $result, $element_key) {
    if ($element_key == 'parent:uid' &&
        $source->feed_nid &&
        ($node = node_load($source->feed_nid))) {
      return $node->uid;
    }
    $item = $result->currentItem();
    return isset($item[$element_key]) ? $item[$element_key] : NULL;
  }

  protected function setup($source_config, FeedsFetcherResult $fetcher_result) {

  }

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $state = $source->state(FEEDS_PARSE);

    $filename = $fetcher_result->getFilePath();

    $context_string = $this->xml_reader_context_string();

    // Count the results if total is empty.
    if (empty($state->total)) {
      $state->total = $this->xml_reader_count_items($filename, $context_string);
    }

    $start = $state->pointer ? $state->pointer : 0;
    $limit = $source->importer->getLimit();
    $total = $state->total;


    $xml = $this->xml_reader_get_items($filename, $context_string, $start, $limit);
    $result = new FeedsParserResult($this->query($xml));
    unset($xml);

    $state->pointer = $start + ($source->importer->getLimit() - $limit);
    $state->progress($state->total, $state->pointer);

    $result->link = $this->link;
    return $result;
  }

  protected function xml_reader_context_string() {
    return $this->source_config['xml_reader_context'];
  }


  /**
   * Helper method to return the number of items matching the context string.
   */
  protected function xml_reader_count_items($filename, $context_string) {

    $context_elements = explode('/', $context_string);

    $xml_reader = new XMLReader();
    $xml_reader->open($filename);

    $tree = new DomDocument();

    $count = 0;
    $count_only = TRUE;
    $offset = 0;
    $limit = -1;

    $this->xml_parser_parse($xml_reader, $tree, $tree, $context_elements, $count, $count_only, '', $offset, $limit);
    $xml_reader->close();
    $tree = NULL;

    return $count;
  }

  /**
   * Helper method to return a range of items as specified in the context string.
   */
  protected function xml_reader_get_items($filename, $context_string, $offset, &$limit) {

    $context_elements = explode('/', $context_string);

    $xml_reader = new XMLReader();
    $xml_reader->open($filename);

    $tree = new DomDocument();

    $count = 0;
    $count_only = FALSE;

    $this->xml_parser_parse($xml_reader, $tree, $tree, $context_elements, $count, $count_only, '', $offset, $limit);
    $xml_reader->close();

    $simple_xml = simplexml_import_dom($tree);
    $tree = NULL;

    return $simple_xml;
  }

  protected function xml_parser_parse(&$xml_reader, &$tree, $current_node, $parent_elements, &$count, $count_only = FALSE, $current_parent_node_name = '', &$offset, &$limit) {
    while ($xml_reader->read()) {
      if ($limit == 0) {
        return;
      }
      switch ($xml_reader->nodeType) {
        case (XMLREADER::ELEMENT):
            // Is this in the chain of parent elements we're trying to get.
            if ((count($parent_elements) > 1) && (strtolower($xml_reader->name) == reset($parent_elements)) && !$xml_reader->isEmptyElement) {
              $new_node = $tree->createElement($xml_reader->name);
              if ($xml_reader->hasAttributes)  {
                while ($xml_reader->moveToNextAttribute()) {
                  $new_node->setAttribute($xml_reader->name, $xml_reader->value);
                }
              }

              $new_parents = $parent_elements;
              $new_current_parent_node_name = array_shift($new_parents);

              $current_node->appendChild($new_node);

              $this->xml_parser_parse($xml_reader, $tree, $new_node, $new_parents, $count, $count_only, $new_current_parent_node_name, $offset, $limit);

            }
            // Is this the element we're trying fetch.
            elseif ((count($parent_elements) == 1) && ($xml_reader->name == reset($parent_elements))) {
              $count++;

              if (!$count_only) {
                if ($offset > 0) {
                  $offset--;
                  // Skip this node.
                  $xml_reader->next();
                }
                else {
                  $limit--;
                  $current_node->appendChild($xml_reader->expand());
                  $xml_reader->next();
                }
              }
              else {
                // Skip this node.
                $xml_reader->next();
              }
            }
            // This is just some other node. I.e. it is a node which is
            // a sibling of an ancestor of the element we are drilling down
            // towards. To allow XPath expressions to target such elements,
            // we add it to our tree, but do not bother reading it's sub-tree.
            // If it contains a simple Text element we *do* include it.
            elseif (!$xml_reader->isEmptyElement) {
              $new_node = $tree->createElement($xml_reader->name);
              if ($xml_reader->hasAttributes)  {
                while ($xml_reader->moveToNextAttribute()) {
                  $new_node->setAttribute($xml_reader->name, $xml_reader->value);
                }
                $xml_reader->moveToElement();
              }
              $textNode = $this->_xml_parser_skip_node($xml_reader);
              if (!empty($textNode)) {
                $new_node->appendChild($textNode);
              }
              $current_node->appendChild($new_node);
            }
        break;
      case (XMLREADER::END_ELEMENT):
        // Is this the closing tag of the parent element.
        if (($xml_reader->name == $current_parent_node_name)) {
          return;
        }
        break;
      }
    }
  }

  // Skip the node completely. This function returns text content if
  // the element contained a text element.
  protected function _xml_parser_skip_node(&$xml_reader) {
    $current_node_name = $xml_reader->name;
    $xml_reader->read();
    $text = FALSE;
    switch ($xml_reader->nodeType) {
      // The node contained a single text node, store its value
      case (XMLREADER::TEXT):
        $text = $xml_reader->expand();
        break;
    }

    // We have popped down into the node and now want to skip all of it
    while (!(($xml_reader->name == $current_node_name) && ($xml_reader->nodeType == XMLREADER::END_ELEMENT))) {
      if (!$xml_reader->next()) {
        break;
      }
    }
    return $text;
  }

}
